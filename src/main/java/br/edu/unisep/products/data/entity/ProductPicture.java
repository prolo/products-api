package br.edu.unisep.products.data.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "product_pictures")
public class ProductPicture {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "url")
    private String url;

}
