package br.edu.unisep.products.data.repository;

import br.edu.unisep.products.data.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {
}
