package br.edu.unisep.products.controller;

import br.edu.unisep.products.domain.dto.ProductDto;
import br.edu.unisep.products.domain.usecase.FindAllProductsUseCase;
import br.edu.unisep.products.domain.usecase.FindProductByIdUseCase;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/product")
public class ProductsController {

    private FindAllProductsUseCase findAllProductsUseCase;
    private FindProductByIdUseCase findProductByIdUseCase;

    @GetMapping
    public ResponseEntity<List<ProductDto>> findAll() {
        var products = findAllProductsUseCase.execute();
        return ResponseEntity.ok(products);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDto> findById(@PathVariable("id") Integer id) {
        var product = findProductByIdUseCase.execute(id);
        return ResponseEntity.ok(product);
    }

}
