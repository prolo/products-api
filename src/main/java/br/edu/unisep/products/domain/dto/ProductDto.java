package br.edu.unisep.products.domain.dto;

import br.edu.unisep.products.data.entity.ProductPicture;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class ProductDto {

    private Integer id;
    private String name;
    private String description;
    private String brand;
    private String productCategory;
    private Double cost_price;
    private Double sale_price;
    private Integer quantity_available;
    private List<ProductPicture> pictures;

}
