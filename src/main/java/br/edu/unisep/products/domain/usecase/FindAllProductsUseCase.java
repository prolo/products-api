package br.edu.unisep.products.domain.usecase;

import br.edu.unisep.products.data.repository.ProductRepository;
import br.edu.unisep.products.domain.builder.ProductBuilder;
import br.edu.unisep.products.domain.dto.ProductDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllProductsUseCase {

    private final ProductRepository productRepository;
    private final ProductBuilder productBuilder;

    public List<ProductDto> execute() {
        var products = productRepository.findAll();

        return productBuilder.from(products);
    }
}
