package br.edu.unisep.products.domain.usecase;

import br.edu.unisep.products.data.repository.ProductRepository;
import br.edu.unisep.products.domain.builder.ProductBuilder;
import br.edu.unisep.products.domain.dto.ProductDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class FindProductByIdUseCase {
    private final ProductRepository productRepository;
    private final ProductBuilder productBuilder;

    public ProductDto execute(Integer id) {
        var product = productRepository.findById(id);

        return product.map(productBuilder::from).orElse(null);
    }
}
