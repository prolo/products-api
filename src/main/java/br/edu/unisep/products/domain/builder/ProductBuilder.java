package br.edu.unisep.products.domain.builder;

import br.edu.unisep.products.data.entity.Product;
import br.edu.unisep.products.domain.dto.ProductDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductBuilder {

    public List<ProductDto> from(List<Product> products) { return products.stream().map(this::from).collect(Collectors.toList()); }

    public ProductDto from(Product products) {
        return new ProductDto(
                products.getId(),
                products.getName(),
                products.getDescription(),
                products.getBrand().getBrand(),
                products.getProductCategory().getDescription(),
                products.getCost_price(),
                products.getSale_price(),
                products.getQuantity_available(),
                products.getProductPictures()
        );
    }
}
